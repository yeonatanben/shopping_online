import { UserModel } from "../models/UserModel";


export class UserState {
    public user: UserModel;
}

export enum UserActionType {
    userDownloaded = "userDownloaded",
    UserAdded = "userAdded",
    userUpdated = "userUpdated",
    userDeleted = "userDeleted"

}

export interface UserAction {
    type: UserActionType;
    payload: any;

}

export function userDownloadedAction(user: UserModel): UserAction {
    return { type: UserActionType.userDownloaded, payload: user };
}
export function userAddedAction(user: UserModel): UserAction {
    return { type: UserActionType.UserAdded, payload: user };
}
export function userUpdatedAction(item: UserModel): UserAction {
    return { type: UserActionType.userUpdated, payload: item };
}
export function userDeletedAction(id: number): UserAction {
    return { type: UserActionType.userDeleted, payload: id };
}



export function userReducer(currentState: UserState = new UserState(), action: UserAction): UserState {

    const newState = { ...currentState };

    switch (action.type) {
        case UserActionType.userDownloaded:
            newState.user = action.payload;
            break;
        case UserActionType.UserAdded:
            newState.user = action.payload;
            break;
        case UserActionType.userUpdated: {
            // const index = newState.user.findIndex(p => p.id === action.payload.id);
            newState.user = action.payload;
            break;
        }
        case UserActionType.userDeleted: {
            // const index = newState.user.findIndex(p => p.id === action.payload);
            newState.user = null;
            break;
        }
    }


    return newState;
}