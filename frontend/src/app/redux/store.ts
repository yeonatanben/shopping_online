import { combineReducers, createStore } from "redux";
import { cartReducer } from "./cart-state";
import { productsReducer } from "./products-state";
import { userReducer } from "./user-state";


// const reducers = combineReducers({ productsState: productsReducer, employeesState: employeesReducer });
const reducers = combineReducers({ productsState: productsReducer, userState:userReducer,cartState:cartReducer});
const store = createStore(reducers);

//// For single reducer
// const store1 = createStore(productsReducer);

export default store;