import { CartModel } from "../models/CartModel";
import { ItemInCartModel } from "../models/ItemInCartModel";


// Cart State: 
export class CartState {
    public items: ItemInCartModel[] = [];
}

// Cart Action Types:
export enum CartActionType {
    itemsDownloaded = "itemsDownloaded",
    itemAdded = "itemAdded",
    itemUpdated = "itemUpdated",
    itemDeleted = "itemDeleted"
}

// Cart Action: 
export interface CartAction {
    type: CartActionType;
    payload: any;
    // More specific type list:
    // payload: Product[] | Product | number;
}

// Cart Action Creators: 
export function cartDownloadedAction(items: ItemInCartModel[]): CartAction {
    return { type: CartActionType.itemsDownloaded, payload: items };
}
export function cartAddedAction(item: ItemInCartModel): CartAction {
    return { type: CartActionType.itemAdded, payload: item };
}
export function cartUpdatedAction(item: ItemInCartModel): CartAction {
    return { type: CartActionType.itemUpdated, payload: item };
}
export function cartDeletedAction(id: number): CartAction {
    return { type: CartActionType.itemDeleted, payload: id };
}

// Cart Reducer:
export function cartReducer(currentState: CartState = new CartState(), action: CartAction): CartState {
    
    const newState = { ...currentState };

    switch(action.type) {
        case CartActionType.itemsDownloaded: // Here payload is all products (ProductModel[])
            newState.items = action.payload;
            break;
        case CartActionType.itemAdded: // Here payload is the added product (ProductModel)
            newState.items.push(action.payload);
            break;
        case CartActionType.itemUpdated: { // Here payload is the updated product (ProductModel)
            const index = newState.items.findIndex(p => p.cartId === action.payload.id);
            newState.items[index] = action.payload;
            break;
        }
        case CartActionType.itemDeleted: { // Here payload is the deleted product's id (number)
            const index = newState.items.findIndex(p => p.cartId === action.payload);
            newState.items.splice(index, 1);
            break;
        }
    }

    return newState;
}