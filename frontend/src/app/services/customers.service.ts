import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ItemInCartModel } from '../models/ItemInCartModel';
import { cartDownloadedAction } from '../redux/cart-state';
import store from '../redux/store';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  BASE_URL = "http://localhost:4000/api/shopping/";
  constructor(private http: HttpClient,private router:Router) { }

  getAllItems() {
    const id = JSON.parse(localStorage.getItem('cartData'));
    this.http.get<ItemInCartModel[]>(this.BASE_URL + "customers/items/" + id)
      .subscribe(value => {
        store.dispatch(cartDownloadedAction(value))
      }
      )
  };

  addItem(newItem: ItemInCartModel) {
    this.http.post<ItemInCartModel>(this.BASE_URL + "customers/item", newItem)
      .subscribe(value => {
        this.getAllItems();
      },err => {
        if(err.status==403){
          alert("Permission expired Re-sign in");
          this.router.navigateByUrl('login')
        }else
        alert('Sorry network error try again later');

      })
  };

  deleteItem(itemId) {
    this.http.delete(this.BASE_URL + "customers/item/" + itemId)
      .subscribe(value => {
        this.getAllItems();
      },err => {
        if(err.status==403){
          alert("Permission expired Re-sign in");
          this.router.navigateByUrl('login')
        }else
        alert('Sorry network error try again later');

      })
  }


  deleteAllItems() {
    const cartId = JSON.parse(localStorage.getItem('cartData'));
    this.http.delete(this.BASE_URL + "customers/cart/" + cartId)
      .subscribe(value => {
        this.getAllItems();
      },err => {
        if(err.status==403){
          alert("Permission expired Re-sign in");
          this.router.navigateByUrl('login')
        }else
        alert('Sorry network error try again later');

      })
  }
}
