import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CartModel } from '../models/CartModel';
import { CategoriesModel } from '../models/CategoriesModel';
import { CitiseModel } from '../models/CitiseModel';
import { OrderModel } from '../models/OrderModel';
import { ProductModel } from '../models/ProductModel';
import { productsDownloadedAction } from '../redux/products-state';
import store from '../redux/store';


@Injectable({
  providedIn: 'root'
})
export class ShoppingService {
  BASE_URL = "http://localhost:4000/api/shopping/";
  constructor(private http: HttpClient, private router: Router) { }



  //מביא מוצר בודד 
  getProductById(id: number): Observable<ProductModel> {
    return this.http.get<ProductModel>(this.BASE_URL + "products/one/" + id);
  }


  //מביא את כל המוצרים
  getAllProducts() {
    this.http.get<ProductModel[]>(this.BASE_URL + "products")
      .subscribe(value => {
        store.dispatch(productsDownloadedAction(value))
      }, err => {
        if (err.status == 403) {
          alert("Permission expired Re-sign in");
          this.router.navigateByUrl('login')
        } else
          alert('Sorry network error try again later');

      })
  };

  // מביא את מספר ההזמנות באתר
  getCountOfOrder(): Observable<number> {
    return this.http.get<number>(this.BASE_URL + "customers/order");
  }


  // מביא את מספר המוצרים
  getCountOfProducts(): Observable<number> {
    return this.http.get<number>(this.BASE_URL + "products/count");
  }
  //מביא מוצרים לפי קטגוריה
  getProductsByCategory(category: number) {
    this.http.get<ProductModel[]>(this.BASE_URL + "products/" + category)
      .subscribe(value => {
        store.dispatch(productsDownloadedAction(value))
      }
      )
  };
  //מביא מוצר לפי שם
  getProductByName(name: string) {
    // store.dispatch(productsDownloadedAction([]))
    this.http.get<ProductModel[]>(this.BASE_URL + "products/name/" + name)
      .subscribe(value => {
        store.dispatch(productsDownloadedAction(value))
      }
      )
  };

  //מביא את כל הקטגוריות-
  getAllCategories(): Observable<CategoriesModel[]> {
    return this.http.get<CategoriesModel[]>(this.BASE_URL + "products/categories");
  }



  //מביא את כל הערים
  getAllCities(): Observable<CitiseModel[]> {
    return this.http.get<CitiseModel[]>(this.BASE_URL + "customers/cities");
  }



  //יוצר עגלה--
  createCart() {
    const id = JSON.parse(localStorage.getItem('loginData')).userId;
    const dt = new Date();
    const date = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();
    const newCart = new CartModel(undefined, id, date, 'true');
    this.http.post<CartModel>(this.BASE_URL + "customers/cart", newCart)
      .subscribe(value => {
        this.http.get<CartModel>(this.BASE_URL + "customers/cartActive/" + id)
          .subscribe(value => {
            localStorage["cartData"] = JSON.stringify(value.cartId);
          })
      });
  }



  //יוצר הזמנה 
  createOrder(newOrder: OrderModel): Promise<any> {
    const userId = JSON.parse(localStorage.getItem('loginData')).userId;
    const cartId = JSON.parse(localStorage.getItem('cartData'))
    const dt = new Date();
    const date = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();
    const dateDelivery = newOrder.dateDelivery;
    const orderDetails = new OrderModel(userId, cartId, this.totalPrice(), newOrder.cityId, newOrder.street, dateDelivery, date, newOrder.payments, undefined);
    return new Promise((resolve, reject) => {
      const order = this.http.post(this.BASE_URL + "customers/order", orderDetails).subscribe
        (value => {
          if (value["message"] == 'The day is full')
            resolve(value)
          else {
            this.http.put(this.BASE_URL + "customers/cart", { "cartId": cartId }).subscribe
              (cart => {
                if (cart) {
                  resolve({ message: "Order complete" });
                }
                else {
                  reject({ message: "Server error" });
                }
              });
          }
        });
    });

  }




  //מביא מוצרים לפי עגלה
  getProductsByCart(id: number): Observable<[]> {
    return this.http.get<[]>(this.BASE_URL + "customers/products/" + id);
  }

  //להביא עגלה לפי ID
  getDataForActiveCart(): Observable<any> {
    let id = JSON.parse(localStorage.getItem('loginData')).userId;
    return this.http.get<CartModel>(this.BASE_URL + "customers/data/cartActive/" + id)
  }
  //להביא הזמנה אחרונה לפי ID
  getLastOrder(): Observable<any> {
    let id = JSON.parse(localStorage.getItem('loginData')).userId;
    return this.http.get<{}>(this.BASE_URL + "customers/orderLast/" + id);
  }




  totalPrice() {
    let sum = 0;
    const items = store.getState().cartState.items
    items.forEach(item => {
      sum += (item.price * item.amount);
    });
    return sum;
  }


}