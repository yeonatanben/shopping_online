
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

        if(localStorage.loginData) {
            request = request.clone({
                setHeaders: {

                    Authorization: "Bearer " + JSON.parse(localStorage.getItem('loginData')).token
                }
            });
        }

        return next.handle(request);
    }
}
