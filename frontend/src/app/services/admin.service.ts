import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  [x: string]: any;
  BASE_URL = "http://localhost:4000/api/shopping/"

  constructor(private http: HttpClient) { }



//מוסיף מוצר
postProduct(fd: FormData): Observable<any> {
  return this.http.post(this.BASE_URL + "AdminArea", fd);
}
//עורך מוצר
editProduct(fd: FormData): Observable<any> {
  return this.http.put(this.BASE_URL + "AdminArea", fd);
}

}
