import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserModel } from '../models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  BASE_URL = "http://localhost:4000/api/shopping/";
  constructor(private http: HttpClient) { }

  //שולח זיהוי ומקבל תוקן  
  login(userData: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const loggedInUser = this.http.post<any>(this.BASE_URL + "auth/login", userData).subscribe
        (userInfo => {
          if (userInfo.userId) {
            localStorage["loginData"] = JSON.stringify(userInfo);
            resolve(userInfo.role);
          }
          else {
            resolve({ message: 'Incorect username or password' });

          }
        },
          err => {
            alert('Sorry network error try again later')
          }
        );
    })
  }

  //יוצר רישום
  registration(userData: UserModel): Promise<any> {
    return new Promise((resolve, reject) => {
      const userReg = this.http.post<UserModel>(this.BASE_URL + "auth/register", userData).subscribe
        (userInfo => {
          if (userInfo) {
            this.login(userData).then(value => {
              resolve(userInfo)
            }
            )
          }
          else {
            resolve("error");
          }
        });
    })

  }

  //בדיקת כפילות ID
  CheckUserId(userId: number) {
    return this.http.get(this.BASE_URL + "auth/check/" + userId);
  }



}
