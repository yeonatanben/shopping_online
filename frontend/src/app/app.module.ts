import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatFormFieldModule } from "@angular/material/form-field";

import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatInputModule } from "@angular/material/input";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout-area/layout/layout.component';
import { MenuComponent } from './components/layout-area/menu/menu.component';
import { HeaderComponent } from './components/layout-area/header/header.component';
import { FooterComponent } from './components/layout-area/footer/footer.component';
import { MainComponent } from './components/layout-area/main/main.component';
import { LoginComponent } from './components/Home/login/login.component';
import { ProductComponent } from './components/product/product.component';
import { AboutComponent } from './components/about/about.component';
import { GeneralInformationComponent } from './components/general-information/general-information.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { Registration2Component } from './components/registration2/registration2.component';
import { ShoppingSiteComponent } from './components/shopping-site/shopping-site.component';
import { OrderComponent } from './components/order/order.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';
import { ActiveCartComponent } from './components/active-cart/active-cart.component';
import { OldOrderComponent } from './components/old-order/old-order.component';
import { JwtInterceptor } from './services/jwt-interceptor.interceptor';
import { CartComponent } from './components/cart/cart.component';
import {MatSliderModule,} from '@angular/material/slider';
import {MatToolbarModule,} from '@angular/material/toolbar';
import {MatSidenavModule,} from '@angular/material/sidenav';
import { ProductsComponent } from './components/products/products.component';
import {MatSelectModule} from '@angular/material/select';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import {MatDialogModule} from '@angular/material/dialog';
import { NewCartComponent } from './components/new-cart/new-cart.component';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { ThumbnailComponent } from './components/thumbnail/thumbnail.component';
import { GetCountComponent } from './components/get-count/get-count.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule, MAT_DATE_LOCALE} from '@angular/material/core';
import { HighlightDirective } from './directives/highlight.directive';









@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    MenuComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    FooterComponent,
    MainComponent,
    LoginComponent,
    ProductComponent,
    AboutComponent,
    GeneralInformationComponent,
    RegistrationComponent,
    Registration2Component,
    ShoppingSiteComponent,
    OrderComponent,
    AddProductComponent,
    PageNotFoundComponent,
    WelcomePageComponent,
    ActiveCartComponent,
    OldOrderComponent,
    CartComponent,
    ProductsComponent,
    AdminPageComponent,
    NewCartComponent,
    EditProductComponent,
    ThumbnailComponent,
    GetCountComponent,
    HighlightDirective,
    
    
    



  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatSliderModule,
    MatToolbarModule,
    MatSidenavModule,
    MatSelectModule,
    MatDialogModule,
    NgxMatFileInputModule,
    MatDatepickerModule,
    MatNativeDateModule
    
  ],
  
  providers: [
    {
    provide: HTTP_INTERCEPTORS, // Register the interceptor
    useClass: JwtInterceptor, // Our interceptor class
    multi: true // Can register it several times if needed
   } ,
   { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
