export class ProductModel {
    constructor(
        public productName: string,
        public categoryId: number,
        public price: number,
        public imgPath?: FileList,
        public productId?: number
    ) { }
    static convertToFormData(product: ProductModel) {
        const fd = new FormData();
        fd.append("productName", product.productName);
        fd.append("categoryId", product.categoryId.toString());
        fd.append("price", product.price.toString());
        if (product.productId)
            fd.append("productId", product.productId.toString());
        if (typeof (product.imgPath) == "object")
            fd.append("imgPath", product.imgPath.item(0));
        return fd;
    }
}

