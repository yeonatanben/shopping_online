export class ItemInCartModel {
    constructor(  
        public amount: number,
        public productId: number,
        public cartId: number,
        public price?:number,
        public imgPath?:string,
        public productName?:string,
        public itemId?: number,
        
    ) { }
}