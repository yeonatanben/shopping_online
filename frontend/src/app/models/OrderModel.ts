import { Data } from "@angular/router";

export class OrderModel {
    constructor(
        
        public userId: number,
        public cartId: number,
        public totalPrice:number,
        public cityId: number,
        public street: string,
        public dateDelivery: Date,
        public dateOrder: string,
        public payments: number,
        public orderId?: number,
    ) { }
}