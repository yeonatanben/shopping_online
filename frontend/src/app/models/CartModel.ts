import { Data } from "@angular/router";

export class CartModel {
    constructor(
        public cartId: number,
        public userId:number,
        public startDate: string,
        public active?: string,
        
    ) { }
}