import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { CartComponent } from './components/cart/cart.component';
import { LoginComponent } from './components/Home/login/login.component';
import { OrderComponent } from './components/order/order.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProductComponent } from './components/product/product.component';
import { ProductsComponent } from './components/products/products.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { Registration2Component } from './components/registration2/registration2.component';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "registration",
    component: RegistrationComponent
  },
  {
    path: "registration2",
    component: Registration2Component
  },
  {
    path: "products",
    component: ProductsComponent
  },
  {
    path: "admin",
    component: AdminPageComponent
  },

  {
    path: "product/:id",
    component: ProductComponent
  },
  {
    path: "about",
    component: AboutComponent
  },
  {
    path: "cart",
    component: CartComponent
  },
  {
    path: "order",
    component: OrderComponent
  },
  {
    path: "",
    redirectTo: "/login",
    pathMatch: "full"
  },
  {
    path: "**",
    component: PageNotFoundComponent
  },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
