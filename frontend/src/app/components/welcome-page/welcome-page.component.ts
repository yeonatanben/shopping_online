import { Component, Input, OnInit } from '@angular/core';
import { ShoppingService } from 'src/app/services/shopping.service';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})

export class WelcomePageComponent implements OnInit {
  @Input()
  username = JSON.parse(localStorage.getItem('loginData')).firstName;
  status = 3;
  date = undefined;
  price = 1;
  


  constructor(private shoppingService: ShoppingService) { }

  ngOnInit(): void {
    this.getData()
  }

  getData(): void {
    this.shoppingService.getDataForActiveCart()
      .subscribe(value => {
        if (value.cartId) {
          this.status = 1;
          this.date = value.startDate;
          this.price = value.totalPrice;
          localStorage["cartData"] = JSON.stringify(value.cartId);
        }
        else {
          this.shoppingService.getLastOrder()
            .subscribe(value => {
              if (value?.cartId) {
                this.status = 2;
                this.date = value.dateOrder;
                this.price =value.totalPrice;
                
              }
            }),
            err=>{
              console.log(err);
              
            }
        }
      });
  }
}
