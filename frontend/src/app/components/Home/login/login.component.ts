import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/models/UserModel';
import { ShoppingService } from 'src/app/services/shopping.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    @ViewChild('f') form: any;
    userDetails: UserModel = new UserModel(undefined, undefined)
    showMassege = false;

    constructor(private authService: AuthService, private http: HttpClient, private router: Router) { }

    ngOnInit(): void {
        localStorage.removeItem('loginData');
        localStorage.removeItem('cartData');
    }

    onLogin() {
        this.authService.login(this.userDetails)
            .then(value => {                
                if (value.message=='Incorect username or password') {
                    this.showMassege = true;
                }
                else {
                    if (value == "admin")
                        this.router.navigateByUrl("admin");
                    else
                        this.router.navigateByUrl("about");
                }
            }).catch(err=>{

                this.showMassege = true;
                
            })
    }


    onClear() {
        this.form.reset();
    }

    onRegister(){
        this.router.navigateByUrl("registration")
    }


}

