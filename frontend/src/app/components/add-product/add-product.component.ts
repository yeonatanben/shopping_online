import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { CategoriesModel } from 'src/app/models/CategoriesModel';
import { ProductModel } from 'src/app/models/ProductModel';
import { ShoppingService } from 'src/app/services/shopping.service';
import { AdminService } from 'src/app/services/admin.service';

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.css']
})
export class AddProductComponent {

    @ViewChild('f') form: any;
    @ViewChild('imageControl') imageControl: ElementRef;
    newProduct: ProductModel = new ProductModel(undefined, undefined, undefined, undefined, undefined,);
    @Output()
    addedItemEvent = new EventEmitter();
    errors: any;
    categories: CategoriesModel[];
    imageVisited: boolean = false;
    public sidebarShow: boolean = true;
    message = undefined;

    constructor(private adminService: AdminService, private shoppingService: ShoppingService) { }


    ngOnInit(): void {
        this.shoppingService.getAllCategories()
            .subscribe(value => this.categories = value);
    }


    onAdd(): void {
        const fd = ProductModel.convertToFormData(this.newProduct);
        this.adminService.postProduct(fd)
            .subscribe(
                (value) => {
                    this.errors = undefined;
                    this.newProduct = new ProductModel(undefined, undefined, undefined, undefined, undefined);
                    this.imageVisited = false;
                    this.imageControl.nativeElement.value = "";
                    this.form.reset();
                    this.shoppingService.getAllProducts();
                    this.addedItemEvent.emit(value);
                },
                err => {
                     console.log(err);
                    this.errors = "error";
                });
    }

    saveImage(args: Event): void {
        this.newProduct.imgPath = (args.target as HTMLInputElement).files;
    }

    imageBlur(): void {
        this.imageVisited = true;
    }

    close(){
        this.addedItemEvent.emit();
    }


}




