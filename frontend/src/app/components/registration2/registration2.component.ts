import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CitiseModel } from 'src/app/models/CitiseModel';
import { UserModel } from 'src/app/models/UserModel';
import store from 'src/app/redux/store';
import { ShoppingService } from 'src/app/services/shopping.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-registration2',
  templateUrl: './registration2.component.html',
  styleUrls: ['./registration2.component.css']
})
export class Registration2Component implements OnInit {
  userDetails: UserModel;
  citise: CitiseModel[];
  showMassege = false;
  constructor(private shoppingService: ShoppingService, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.userDetails = store.getState().userState.user;
    console.log(this.userDetails);

    this.shoppingService.getAllCities()
      .subscribe(value => this.citise = value);
  }

  onRegistration() {
    this.authService.registration(this.userDetails).then
      (value => {
        console.log(value);
        
        this.router.navigateByUrl("about");
      }).catch(err => {
        console.log(err);

      })

  }
}
