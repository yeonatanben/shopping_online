import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CitiseModel } from 'src/app/models/CitiseModel';
import { ItemInCartModel } from 'src/app/models/ItemInCartModel';
import { OrderModel } from 'src/app/models/OrderModel';
import store from 'src/app/redux/store';
import { ShoppingService } from 'src/app/services/shopping.service';
import { CustomersService } from 'src/app/services/customers.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  inOrder=true;
  items: ItemInCartModel[];
  itemSearch = undefined;
  date = new Date();
  orderDetails: OrderModel = new OrderModel(undefined, undefined, undefined, undefined, undefined, this.date, undefined, undefined);
  order = false;
  cities: CitiseModel[];
  minDate = new Date();
  OrderComplete = false;
  orderFild=false;
  fileUrl;
  messageServer=false;
  content = 'Angular highlight text';
  imageUrl = "http://localhost:4000/";
  constructor(private customersService: CustomersService, private sanitizer: DomSanitizer, public shoppingService: ShoppingService, private router: Router) { }

  ngOnInit(): void {
    this.customersService.getAllItems();
    this.shoppingService.getAllCities()
      .subscribe(value => this.cities = value);
    store.subscribe(() => {
      this.items = store.getState().cartState.items;
      this.generateReciept();
    }); 
  }

  generateReciept()
  {
    let data = 'reciept\n\n';
    this.items.forEach(element => {
      data += element.productName + " * " + element.amount + " = " + element.amount*element.price + "\n";
    });
    data += "Total: " + this.shoppingService.totalPrice() + "\n";

    const blob = new Blob([data], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  myFilter = (d: Date | null): boolean => {
    const day = (d || new Date());
    return day < new Date();
  };


  back() {
    this.router.navigateByUrl("products");
  }

  toPay() {
    this.order = true;
  }

  search() {
    // this.router.navigateByUrl("products");
  }

  onClick() {

    this.shoppingService.createOrder(this.orderDetails).then
      (value => {
        if (value.message =='Order complete' ){
          this.OrderComplete = true;
          this.order=false;
          this.orderFild=false;
          this.inOrder=false;
        }
        else{
          this.orderFild = true;
          this.OrderComplete = false;
        }
          
      }).catch
      (error => {
        this.messageServer = true;
      }
      );

  }

  eventEmitDoubleClickCity(event) {
    let city = JSON.parse(localStorage.getItem('loginData')).cityId;
    this.orderDetails.cityId = city;
  }

  eventEmitDoubleClickStreet(event) {
    let street = JSON.parse(localStorage.getItem('loginData')).street;
    this.orderDetails.street = street;
  }

  
  send() {
    this.router.navigateByUrl("")
  }
}



