import { Component, OnInit } from '@angular/core';
import { ShoppingService } from 'src/app/services/shopping.service';

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.component.html',
  styleUrls: ['./general-information.component.css']
})
export class GeneralInformationComponent implements OnInit {
  countProducts:{};
  countOrders:{};
  constructor(private shoppingService: ShoppingService) { }

  ngOnInit(): void {
    this.getCounts()
  }

  getCounts(): void {
    this.shoppingService.getCountOfProducts()
      .subscribe(value => 
        this.countProducts = value
      );
    this.shoppingService.getCountOfOrder()
      .subscribe(value =>
        this.countOrders = value
      );
  }

}
