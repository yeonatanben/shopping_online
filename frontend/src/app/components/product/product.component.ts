import { Component, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductModel } from 'src/app/models/ProductModel';
import { ShoppingService } from 'src/app/services/shopping.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
   product:ProductModel;
  // @Input product:any;
  imageUrl="http://localhost:4000/";
  constructor(private shoppingService:ShoppingService, private activatedRoute:ActivatedRoute) { }

  ngOnInit():void {
    const id=+this.activatedRoute.snapshot.params.id;
    this.shoppingService.getProductById(id)
      .subscribe(value =>
        this.product = value
      
      );
  }
 

}
