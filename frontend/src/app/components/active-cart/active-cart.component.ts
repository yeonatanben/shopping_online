import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-active-cart',
  templateUrl: './active-cart.component.html',
  styleUrls: ['./active-cart.component.css']
})

export class ActiveCartComponent implements OnInit {
  
 
  @Input()
  date: string;
  @Input()
  price: number;
  constructor(private router:Router) { }

  ngOnInit(): void {

  }

  next(){
    this.router.navigateByUrl("products");
  }

}
