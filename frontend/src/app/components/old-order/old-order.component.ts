import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShoppingService } from 'src/app/services/shopping.service';

@Component({
  selector: 'app-old-order',
  templateUrl: './old-order.component.html',
  styleUrls: ['./old-order.component.css']
})
export class OldOrderComponent implements OnInit {
  @Input()
  date: string;
  @Input()
  price: number;
  constructor(private router:Router,private shoopingService:ShoppingService) { }

  ngOnInit(): void {
  }

  start(){
    this.router.navigateByUrl("products")
    this.shoopingService.createCart();
  }
}
