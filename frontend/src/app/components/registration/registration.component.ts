import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/models/UserModel';
import store from 'src/app/redux/store';
import { userAddedAction, userDownloadedAction } from 'src/app/redux/user-state';
import { ShoppingService } from 'src/app/services/shopping.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  @ViewChild('f') form: any;
  userData = new UserModel(undefined, undefined, undefined)
  password: string;
  message=false;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {

  }

  onSend() {
    if(this.password==this.userData.password){
    this.authService.CheckUserId(this.userData.userId)
      .subscribe(
        data => {
          if (data == 'excellent') {
            store.dispatch(userAddedAction(this.userData))
            this.router.navigateByUrl("registration2")
          }
          else {
            this.message = true
          }
        },
        err => console.log(err),
      );
  }
}

  onClear() {
    this.form.reset();
}
}
