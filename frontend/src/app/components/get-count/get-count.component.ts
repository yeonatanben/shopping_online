import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemInCartModel } from 'src/app/models/ItemInCartModel';
import { ShoppingService } from 'src/app/services/shopping.service';
import { CustomersService } from 'src/app/services/customers.service';

@Component({
	selector: 'app-get-count',
	templateUrl: './get-count.component.html',
	styleUrls: ['./get-count.component.css']
})
export class GetCountComponent implements OnInit {
	amount = 1;
	constructor(@Inject(MAT_DIALOG_DATA) public data: {productId:number,  productName: string }, private shoppingService: ShoppingService,private customersService:CustomersService) { }

	ngOnInit(): void {
	
	}
	
	onAdd() {
		
		const cartId = JSON.parse(localStorage.getItem('cartData'));
		const newItem = new ItemInCartModel(this.amount, this.data.productId, cartId, undefined);
		this.customersService.addItem(newItem);
	}

}
