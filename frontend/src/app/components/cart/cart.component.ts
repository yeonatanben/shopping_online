
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ItemInCartModel } from 'src/app/models/ItemInCartModel';
import store from 'src/app/redux/store';
import { ShoppingService } from 'src/app/services/shopping.service';
import { CustomersService } from 'src/app/services/customers.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})

// import {Component} from '@angular/core';

// /** @title Drawer with explicit backdrop setting */
// @Component({
//   selector: 'sidenav-backdrop-example',
//   templateUrl: 'sidenav-backdrop-example.html',
//   styleUrls: ['sidenav-backdrop-example.css'],
// })
// export class SidenavBackdropExample {}

export class CartComponent implements OnInit {
  name = JSON.parse(localStorage.getItem('loginData')).firstName;
  show = false;
  items: ItemInCartModel[];
  imageUrl = "http://localhost:4000/";
  public sidebarShow: boolean = true;
  constructor(public shoppingService:ShoppingService, private customersService: CustomersService , private router: Router) { }

  ngOnInit(): void {
    this.getProductsInCart();
    store.subscribe(() => {
      this.items = store.getState().cartState.items;
    });
  }
  getProductsInCart(): void {
    this.customersService.getAllItems();
  }

  back() {
    this.router.navigateByUrl("products");
  }

  deleteItem(itemId) {
    this.customersService.deleteItem(itemId);
  }

  deleteAll() {
    this.customersService.deleteAllItems();
  }

    toOrder(){
    this.router.navigateByUrl("order");
  }

}
