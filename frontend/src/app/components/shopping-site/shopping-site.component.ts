import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CategoriesModel } from 'src/app/models/CategoriesModel';
import { ProductModel } from 'src/app/models/ProductModel';
import store from 'src/app/redux/store';
import { ShoppingService } from 'src/app/services/shopping.service';

@Component({
  selector: 'app-shopping-site',
  templateUrl: './shopping-site.component.html',
  styleUrls: ['./shopping-site.component.css']
})
export class ShoppingSiteComponent implements OnInit {
  productSearch: string;
  find = false;
  categories :CategoriesModel[];
  selectdCategory = 0;
  constructor(private shoppingService: ShoppingService, private router: Router) { }

  ngOnInit(): void {
    this.shoppingService.getAllCategories()
      .subscribe(value => this.categories = value);
    
  }

  search(productSearch: string): void {
    this.shoppingService.getProductByName(productSearch);
    this.productSearch="";
    this.find = true;

  }
 

  back(): void {
    this.shoppingService.getAllProducts();
    this.selectdCategory=0;
    this.find = false;
  }

  cart() {
    this.router.navigateByUrl("cart")
  }

  setCategory(categoryId: number) {
    this.shoppingService.getProductsByCategory(categoryId);
    this.selectdCategory = categoryId;
    this.find = true;
  }
  logOut(){
    this.router.navigateByUrl('login')
  }



}
