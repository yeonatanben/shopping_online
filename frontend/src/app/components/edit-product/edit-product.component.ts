import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriesModel } from 'src/app/models/CategoriesModel';
import { ProductModel } from 'src/app/models/ProductModel';
import { ShoppingService } from 'src/app/services/shopping.service';
import { AdminService } from 'src/app/services/admin.service';

@Component({
    selector: 'app-edit-product',
    templateUrl: './edit-product.component.html',
    styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

    @ViewChild('f') form: any;
    @ViewChild('imageControl') imageControl: ElementRef;
    @Input()
    productToEdit: ProductModel = new ProductModel(undefined, undefined, undefined,);
    @Output()
    editedItemEvent = new EventEmitter<string>();
    errors: any;
    categories: CategoriesModel[];
    imageVisited: boolean = false;
    //message = false;
    public sidebarShow: boolean = true;
    imageUrl = "http://localhost:4000/";

    constructor(private adminService: AdminService, private shoppingService: ShoppingService, private router: Router) { }


    ngOnInit(): void {
        this.shoppingService.getAllCategories()
            .subscribe(value => this.categories = value);

    }


    onPush(): void {
        const fd = ProductModel.convertToFormData(this.productToEdit);
        this.adminService.editProduct(fd)
            .subscribe(
                (value) => {
                    this.sidebarShow = false;
                    this.shoppingService.getAllProducts();
                    this.editedItemEvent.emit(value);
                },
                err => {
                    this.errors = "error";
                });
    }

    saveImage(args: Event): void {
        this.productToEdit.imgPath = (args.target as HTMLInputElement).files;
    }

    imageBlur(): void {
        this.imageVisited = true;
    }

    close(){
        this.editedItemEvent.emit();
    }


}