import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShoppingService } from 'src/app/services/shopping.service';

@Component({
  selector: 'app-new-cart',
  templateUrl: './new-cart.component.html',
  styleUrls: ['./new-cart.component.css']
})
export class NewCartComponent implements OnInit {

  constructor(private router:Router,private shoopingService:ShoppingService) { }

  ngOnInit(): void {
  }

  start(){
    this.router.navigateByUrl("products")
    this.shoopingService.createCart();
  }
}
