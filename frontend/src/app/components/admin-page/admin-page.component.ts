import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriesModel } from 'src/app/models/CategoriesModel';
import { ProductModel } from 'src/app/models/ProductModel';
import store from 'src/app/redux/store';
import { ShoppingService } from 'src/app/services/shopping.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {
  userName = JSON.parse(localStorage.getItem('loginData')).firstName;
  products: ProductModel[] = undefined;
  categories: CategoriesModel[];
  productToEdit = new ProductModel(undefined, undefined, undefined)
  toAdd = false;
  show = false;
  imageUrl = "http://localhost:4000/";
  constructor(private shoppingService: ShoppingService,private router:Router) { }

  ngOnInit(): void {
    this.getProducts();
    this.shoppingService.getAllCategories()
      .subscribe(value => this.categories = value);
    store.subscribe(() => {
      this.products = store.getState().productsState.products;
    });
  }

  getProducts(): void {
    this.shoppingService.getAllProducts();
  }

  onEdit(product: ProductModel) {
    this.show = true;
    this.productToEdit.price = product.price;
    this.productToEdit.productName = product.productName;
    this.productToEdit.imgPath = product.imgPath;
    this.productToEdit.categoryId = product.categoryId;
    this.productToEdit.productId = product.productId;
  }

  itemEdited(){
    this.show =false;
  }
  itemAdded(){
    this.toAdd =false;
  }

  add() {
    this.toAdd = true;
    }
  logOut(){
    this.router.navigateByUrl('login')
  }

}
