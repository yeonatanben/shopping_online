import { Component, OnInit } from '@angular/core';
import { CartModel } from 'src/app/models/CartModel';
import { ItemInCartModel } from 'src/app/models/ItemInCartModel';
import { ProductModel } from 'src/app/models/ProductModel';
import store from 'src/app/redux/store';
import { ShoppingService } from 'src/app/services/shopping.service';
import { MatDialog } from '@angular/material/dialog';
import { GetCountComponent } from '../get-count/get-count.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: ProductModel[];
  cart: CartModel = new CartModel(undefined, undefined, undefined, undefined)
  // @Input product:any;
  imageUrl = "http://localhost:4000/";
  amount = 1;
  constructor(private shoppingService: ShoppingService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getProducts();
    store.subscribe(() => {
      this.products = store.getState().productsState.products;
    });
  }


  getProducts(): void {
    this.shoppingService.getAllProducts();

  }

  SelectQuantity() {
    this.dialog.open(GetCountComponent);
  }
  onPush(product) {
    this.dialog.open(GetCountComponent, {
      data: { productId: product.productId, productName: product.productName },
    });
    
  }

  





}
