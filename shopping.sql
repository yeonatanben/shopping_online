-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2022 at 01:38 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopping`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `cartId` int(4) NOT NULL,
  `userId` int(10) NOT NULL,
  `startDate` date NOT NULL,
  `active` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`cartId`, `userId`, `startDate`, `active`) VALUES
(110, 456, '2022-01-31', 'false'),
(111, 456, '2022-01-21', 'false'),
(112, 2, '2022-01-31', 'false'),
(114, 456, '2022-01-31', 'false'),
(116, 456, '2022-01-31', 'false'),
(120, 456, '2022-01-31', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `categoryId` int(4) NOT NULL,
  `categoryName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryId`, `categoryName`) VALUES
(1, 'Milk'),
(2, 'Fruits and vegetable'),
(3, 'Snicks'),
(4, 'Fish'),
(5, 'Dry products'),
(6, 'Bread and pastries'),
(7, 'Drinks'),
(8, 'Surprise');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `cityId` int(4) NOT NULL,
  `cityName` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`cityId`, `cityName`) VALUES
(11, 'Hifa'),
(22, 'Beer-Sheva'),
(27, 'Herzliya'),
(33, 'Jerusalem'),
(35, 'Rehovot'),
(41, 'Tiberias'),
(45, 'Natany'),
(50, 'Tel aviv'),
(53, 'Safed'),
(60, 'Ashdod');

-- --------------------------------------------------------

--
-- Table structure for table `itemincart`
--

CREATE TABLE `itemincart` (
  `itemId` int(4) NOT NULL,
  `productId` int(8) NOT NULL,
  `amount` int(3) NOT NULL,
  `cartId` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `itemincart`
--

INSERT INTO `itemincart` (`itemId`, `productId`, `amount`, `cartId`) VALUES
(310, 57, 1, 110),
(311, 61, 1, 110),
(312, 60, 3, 110),
(313, 56, 7, 110),
(314, 58, 13, 112),
(315, 59, 15, 112),
(316, 56, 1, 112),
(317, 56, 13, 111),
(318, 58, 1, 111),
(319, 87, 2, 111),
(321, 60, 50, 114),
(322, 56, 1, 114),
(324, 56, 11, 116),
(325, 58, 5, 116),
(326, 64, 1, 116),
(335, 56, 1, 120);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderId` int(4) NOT NULL,
  `userId` int(4) NOT NULL,
  `cartId` int(4) NOT NULL,
  `totalPrice` int(10) NOT NULL,
  `cityId` int(4) NOT NULL,
  `street` varchar(15) NOT NULL,
  `dateDelivery` date NOT NULL,
  `dateOrder` date NOT NULL,
  `payments` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderId`, `userId`, `cartId`, `totalPrice`, `cityId`, `street`, `dateDelivery`, `dateOrder`, `payments`) VALUES
(95, 456, 110, 60, 11, 'bochrim', '2022-02-23', '2022-01-31', 4344),
(96, 2, 112, 86, 45, 'wer', '2022-01-31', '2022-01-31', 7543),
(97, 456, 111, 110, 11, 'bochrim', '2026-02-02', '2022-01-31', 5665),
(99, 456, 114, 254, 11, 'bochrim', '2022-02-23', '2022-01-31', 3321),
(100, 456, 116, 97, 11, 'bochrim', '2022-01-31', '2022-01-31', 6422);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productId` int(8) NOT NULL,
  `productName` varchar(50) NOT NULL,
  `categoryId` int(4) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `imgPath` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productId`, `productName`, `categoryId`, `price`, `imgPath`) VALUES
(56, 'bisli', 3, '4.30', 'e2fb6237-091b-4d4c-96c8-abf93868c980.jpg'),
(57, 'Milk', 1, '4.00', '7a7d037f-3bac-4d14-81a3-02329918d8c5.jpg'),
(58, 'Milki', 1, '4.00', '792edc2a-7a0e-43d1-a48a-4aa64ebf6973.jpg'),
(59, 'Banana', 2, '2.00', '3937e324-bde4-4893-a3f2-8df035d2e4db.jpg'),
(60, 'Bamba', 3, '5.00', 'be069cdd-4040-457c-9908-24480819131d.jpg'),
(61, 'Grapes', 2, '12.00', '59608391-dd06-45e1-9cfa-577becc5b61f.jpg'),
(62, 'Avokado', 2, '15.00', '7bc738d6-bb58-4d3a-8594-6d29084616cf.jpg'),
(63, 'Coka cola', 7, '10.00', '6fba0ccc-bb04-403c-a761-f2641e834304.jpg'),
(64, 'Honey', 5, '30.00', '96d63114-aae1-44ae-9047-0c097c765e50.jpg'),
(65, 'Cheese', 1, '15.00', '590ccabf-9d3a-4f9c-94b5-52f5d47145d8.jpg'),
(66, 'Pasta', 5, '5.00', '81dfa715-5187-49ba-a2b3-3a78b644f8ca.jpg'),
(67, 'Ptitim', 5, '4.50', 'fd5234a5-6944-4bad-aee9-5283de5cf42e.jpg'),
(68, 'Spring Apple flavor', 7, '8.00', '019e25d7-56b1-40a1-9100-23b34886ad5d.jpg'),
(69, 'Chocolate pastry', 6, '20.00', 'fd9b1190-ab71-4539-92bf-1781732d3388.jpg'),
(70, 'Amnon fish', 4, '30.00', '964828a0-78ea-42d2-b02c-e811566c1b52.jpg'),
(71, 'Denis fish', 4, '40.00', '3c6d6b69-f486-42fb-8250-67ab952c2491.jpg'),
(72, 'Salomon fish', 4, '51.00', '831aaae5-b8a6-42bc-91d6-3bcb513aa492.jpg'),
(73, 'Tona', 4, '7.00', '4fdce4bd-fe50-4322-9115-c37ec67b9bab.jpg'),
(74, 'Carranz Chocolate', 3, '7.00', '1113c8c1-5d9f-4d07-8aef-e054e27f2b05.jpg'),
(75, 'rogalch', 6, '7.00', '78159da9-adf9-4606-a14a-57b4acaba293.jpg'),
(76, 'Sliced bread', 6, '11.00', '7d06dc9e-557b-4151-816b-ab3c9c1d63b5.jpg'),
(77, 'Potato', 2, '4.00', '1763b3bc-a6ea-47e3-957e-51ce45ca1d96.jpg'),
(79, 'Doritos', 3, '3.00', '9f8bffcd-d8d3-4d00-a08d-fc8cec64cef3.jpg'),
(87, 'Eggs', 5, '25.90', '2c3335a8-5e8a-452f-ad0c-81641d162296.jpg'),
(90, 'dfghj', 7, '6.70', '64ca5bb6-8eac-4429-b054-f819607c1459.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(5) NOT NULL,
  `firstName` varchar(15) NOT NULL,
  `lastName` varchar(15) NOT NULL,
  `mail` varchar(40) NOT NULL,
  `password` varchar(150) NOT NULL,
  `cityId` int(4) NOT NULL,
  `street` varchar(30) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `firstName`, `lastName`, `mail`, `password`, `cityId`, `street`, `role`) VALUES
(2, 'Michal', 'Cohen', 'hhh@gg.com', '01c7ca53825819185aba0c8c925af7363fc3fae7f5dda76e62c4a4f3d61fdfb0914706ef784e7a3b9de6bad4729d3fbe98cb733a53bc0cf4cd77ca7e7e017713', 45, 'wer', ''),
(455, 'yoni', 'ben', 'ben@walla.com', '57fba9484b325ea1130a864fb45a711c71d2502b26297a39d14aed4e43655f8d118d09c37d6f8b81f1db4a721c8bfd4105393e4b2976d0530619cee4f7d21caa', 33, 'pisga', ''),
(456, 'rivka', 'ben', 'rivka@com', 'a088498eb4b678011787c6a295e2b70ac8f5b026aa7c023ca861b6f14e22d9bb9182959e01f4ee6079fbae1ff578e328acd10cf843b81e36301035e1814a3ea6', 11, 'bochrim', ''),
(3460220, 'riki', 'com', 'riki@com', '0b956111d0a8648818938bed873c3e944c970054fa9f78f83f4ceea02860aba1141e89b9efc2351e16ae9f101c0179865c1d0d33b14775bfaa314dfc82fb5185', 53, 'hari 15', ''),
(66430471, 'yoni', 'ben hur', 'yoni@ben.com', '0e15ef68db4045dbe3486e85350eabbc03fc77d42bd053dedca615c2df7bb040a00fae1b01144ac8908067ad90d13539b16a9d2992c6e10a8166d3b9f9ab9e1e', 11, '', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`cartId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`cityId`);

--
-- Indexes for table `itemincart`
--
ALTER TABLE `itemincart`
  ADD PRIMARY KEY (`itemId`),
  ADD KEY `cartId` (`cartId`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderId`),
  ADD KEY `cartId` (`cartId`),
  ADD KEY `cityId` (`cityId`),
  ADD KEY `cityId_2` (`cityId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `cityId_3` (`cityId`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productId`),
  ADD KEY `categoryId` (`categoryId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD KEY `city` (`cityId`),
  ADD KEY `cityId` (`cityId`),
  ADD KEY `cityId_2` (`cityId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `cartId` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `itemincart`
--
ALTER TABLE `itemincart`
  MODIFY `itemId` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=336;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderId` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productId` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `itemincart`
--
ALTER TABLE `itemincart`
  ADD CONSTRAINT `itemincart_ibfk_1` FOREIGN KEY (`cartId`) REFERENCES `carts` (`cartId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `itemincart_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`cartId`) REFERENCES `carts` (`cartId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`cityId`) REFERENCES `cities` (`cityId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`cityId`) REFERENCES `cities` (`cityId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
