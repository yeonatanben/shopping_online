const Joi = require("joi");

class OrderModel {

    constructor(order) {
        this.orderId = order.orderId;
        this.userId = order.userId;
        this.cartId = order.cartId;
        this.totalPrice = order.totalPrice;
        this.cityId = order.cityId;
        this.street = order.street;
        this.dateDelivery = order.dateDelivery;
        this.dateOrder = order.dateOrder;
        this.payments = order.payments.toString().slice(-4);
        

    }

    
    // static #validationSchema = Joi.object({
    //     orderId: Joi.string().allow(),
    //     userId: Joi.string().allow(),
    //     cartId: Joi.string().allow(),
    //     totalPrice: Joi.string().allow(),
    //     cityId: Joi.string().required().min(2).max(15),
    //     street: Joi.string().required().min(2).max(15),
    //     dateDelivery: Joi.string().required().min(2).max(15),
    //     dateOrder: Joi.string().required().min(2).max(15),
    //     payments: Joi.string().required().min(2).max(15),

    // });

    // validate() {

    //     const result = Registration.#validationSchema.validate(this, { abortEarly: false });
    //     const errObj = {};
    //     if (result.error) {
    //         result.error.details.map(err => {
    //             errObj[err.path[0]] = err.message;
    //         });
    //         return errObj;
    //     }
    //     return this.null;
    // }
}

module.exports =OrderModel;