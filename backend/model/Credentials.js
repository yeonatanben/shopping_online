const joi = require("joi");

class Credentials {
    constructor(credentials) {
        this.email = credentials.email;
        this.password = credentials.password;
    }
    static #validationScheme = joi.object({
        email: joi.string().required().min(2).max(15),
        password: joi.string().required().min(4).max(15)
    });
    validate(){
        const result=Credentials.#validationScheme.validate(this, {abortEarly:false})
        return result.error ? result.error.details.map(err=> err.message):null;
    }
}


module.exports= Credentials;
