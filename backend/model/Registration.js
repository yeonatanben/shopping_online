const Joi = require("joi");

class Registration {

    constructor(registration) {
        this.userId = registration.userId;
        this.firstName = registration.firstName;
        this.lastName = registration.lastName;
        this.password = registration.password;
        this.email = registration.email;
        this.cityId = registration.cityId;
        this.street = registration.street;

    }

    static #validationSchema = Joi.object({
        firstName: Joi.string().required().min(2).max(10),
        lastName: Joi.string().required().min(2).max(10),
        userId: Joi.string().required().min(2).max(9),
        password: Joi.string().required().min(4).max(15),
        street: Joi.string().required().min(2),
        email: Joi.string().required().min(2),
        cityId:Joi.allow()

    });

    validate() {

        const result = Registration.#validationSchema.validate(this, { abortEarly: false });
        const errObj = {};
        if (result.error) {
            result.error.details.map(err => {
                errObj[err.path[0]] = err.message;
            });
            return errObj;
        }
        return this.null;
    }
}

module.exports = Registration;