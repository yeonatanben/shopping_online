const Joi = require("joi");

class ItemInCartModel {

    constructor(itemincart) {
        this.itemId = itemincart.itemId;
        this.productId = itemincart.productId;
        this.amount = itemincart.amount;
        this.cartId = itemincart.cartId;
      

    }

    
}

module.exports =ItemInCartModel;