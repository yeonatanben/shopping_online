const { boolean } = require("joi");
const Joi = require("joi");
class ProductModel {
    constructor(productName,
        categoryId, price, imgPath) {
      
         if (arguments.length === 1) {
            const product = arguments[0];
            if(product.productId)
            this.productId=product.productId;
            this.productName = product.productName;
            this.categoryId = product.categoryId;
            this.price = product.price;
            this.imgPath = product.imgPath;
        }
        else
            throw "productModel structure error";
    }

    static #validationScheme = Joi.object({
        productId: Joi.allow(),
        productName: Joi.string().required().min(2),
        categoryId: Joi.required(),
        price: Joi.number().required(),
        imgPath:Joi.allow()
    });
    validate() {
        const result = ProductModel.#validationScheme.validate(this, { abortEarly: false });
        return result.error.details;
    }
}
module.exports = ProductModel;