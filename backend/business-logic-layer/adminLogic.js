const dal = require("../data-access-layer/dal");
const { builtinModules } = require("module");
const path = require("path");
const { v4: uuidv4 } = require('uuid');
const { request } = require("express");









function postProduct(product) {
    const newProduct = dal.executeQueryAsync(`insert into products values(null,?,?,?,?)`, [product.productName, product.categoryId, product.price, product.imgPath]);
    return newProduct;
}

async function handleImage(image) {
    const imgUnique = uuidv4();
    let file_extension = image.name.substring(image.name.lastIndexOf('.') + 1);
    let file_name = imgUnique + '.' + file_extension;
    const absolutePath = path.join(__dirname, "..", "images", file_name);
    await image.mv(absolutePath);
    return file_name;
}

function editProduct(product) {

    let sql;
    if (product.imgPath) {
        sql = `UPDATE products SET
        productId='${product.productId}',
        productName='${product.productName}',
        categoryId='${product.categoryId}',
        price='${product.price}',
        imgPath='${product.imgPath}'
         WHERE productId=${product.productId}`;
    }
    else {
        sql = `UPDATE products SET
        productId='${product.productId}',
        productName='${product.productName}',
        categoryId='${product.categoryId}',
        price='${product.price}'
         WHERE productId=${product.productId}`;
    }

    const updataProduct =
        dal.executeQueryAsync(sql);
    return updataProduct;
};




module.exports = {

    postProduct,
    editProduct,
    handleImage

}