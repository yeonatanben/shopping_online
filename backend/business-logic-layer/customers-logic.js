const dal = require("../data-access-layer/dal");


//קבלת כל הערים
function getAllCitiesAsync() {
    const cities = dal.executeQueryAsync("select * from cities");
    return cities;
}
//קבלת עגלה לפי יוזר
function getCartByIdAsync(id) {
    const cart = dal.executeQueryAsync(`SELECT * FROM carts WHERE  userId=${id} and active='yes'`);
    return cart;
}

//יצירת עגלה
function insertCartAsync(cart) {

    const insertCart = dal.executeQueryAsync(`INSERT INTO carts values(null,?, ?, ?)`, [cart.userId, cart.startDate, cart.active]);
    return insertCart;
}
//עדכון סטטוס עגלה
function updateCartAsync(id) {
    const voidCart = dal.executeQueryAsync(`UPDATE carts SET active='false' WHERE cartId=${id}`);
    return voidCart;

}
//הוספת מוצר לעגלה     
async function insert(item) {
    let insertItemInCart;
    const productId = await dal.executeQueryAsync(`select * from itemincart where cartId = ${item.cartId} and productId =${item.productId}`);
    if (!productId || productId.length < 1)
        return false;
    return true;

}

//הוספת מוצר לעגלה     
function insertItemInCartAsync(item) {
    const insertItemInCart = dal.executeQueryAsync(`INSERT INTO itemincart values (null, ?, ?, ?)`, [item.productId, item.amount, item.cartId]);
    return insertItemInCart;
}
//   ע--עריכת מוצר--מר
function updateItemInCartAsync(item) {
    const updateItemInCart = dal.executeQueryAsync(`UPDATE itemincart SET amount='${item.amount}' WHERE cartId='${item.cartId}' and productId='${item.productId}'`);
    return updateItemInCart;

}


//הבאת מספר ההזמנות באתר
function getcountOrderAsync() {
    const countOrder = dal.executeQueryAsync(`SELECT COUNT(orderId) as sumorder FROM orders`);
    return countOrder;
}

//הזמנה חדשה-
function insertOrderAsync(order) {
    const insertOrder = dal.executeQueryAsync(`INSERT INTO orders values (null, ?, ?, ?, ?, ?, ?, ?, ?)`, [order.userId, order.cartId, order.totalPrice, order.cityId, order.street, order.dateDelivery, order.dateOrder, order.payments]);
    return insertOrder;

}

//הבאת כל המוצרים שבעגלה מסויימת 

function getProductsBycartAsync(cartId) {
    const productsByCart = dal.executeQueryAsync(`SELECT * FROM itemincart WHERE cartId=${cartId}`);
    return productsByCart;
}
//מביא את כל המוצרים(מעגלה)
function getAllItemsAsync(id) {
    const items = dal.executeQueryAsync(`SELECT itemincart.itemId, itemincart.amount,products.productName,products.price,products.imgPath 
    FROM itemincart JOIN products ON
    itemincart.productId=products.productId
    WHERE itemincart.cartId=${id}`);
    return items;
}
//הבאת  מידע על עגלה פעילה
function getActiveCartDataAsync(id) {
    // SELECT * FROM carts WHERE userId=${id} AND active='true'
    const activeCart = dal.executeQueryAsync(`
    SELECT carts.cartId,carts.startDate, carts.userId,sum(products.price*itemincart.amount)as totalPrice
    FROM carts JOIN itemincart ON
    itemincart.cartId = carts.cartId
    JOIN products on itemincart.productId=products.productId
    WHERE carts.userId = ${id} and carts.active = "true"
    `);

    if (activeCart.length < 1)
        return null;
    return activeCart;
}
// לפי יוזר הבאת  עגלה פעילה
function getActiveCartAsync(id) {

    const activeCart = dal.executeQueryAsync(`
    SELECT * FROM carts WHERE userId=${id} AND active='true'
    `);
    if (activeCart.length < 1)
        return null;
    return activeCart;
}

//הבאת עגלה ישנה
function getOldCartAsync(id) {
    const oldCart = dal.executeQueryAsync(`
    SELECT carts.cartId, orders.dateOrder,orders.totalPrice
    FROM carts JOIN orders ON
    orders.cartId = carts.cartId
    WHERE carts.userId =? and carts.active = "false"
    ORDER by startDate DESC`
        , [id]
    );
    if (oldCart.length < 1)
        return null
    return oldCart;
}




function deleteItemAsync(id) {
    const deleteItem = dal.executeQueryAsync(`DELETE FROM itemincart WHERE itemincart.itemId ='${id}'`);
    return deleteItem;
}

function deleteAllItemsAsync(id) {
    const deleteItems = dal.executeQueryAsync(`DELETE FROM itemincart WHERE cartId=${id}`);
    return deleteItems;
}


async function CheckCountDeliveryInDay(date) {
    const dayFull = await dal.executeQueryAsync(
        `SELECT * FROM orders WHERE dateDelivery='${date}'`
    );

    if (dayFull.length > 2) return true;
    return false;
}















module.exports = {
    getAllCitiesAsync,
    insertItemInCartAsync,
    updateItemInCartAsync,
    getCartByIdAsync,
    insertCartAsync,
    getcountOrderAsync,
    insertOrderAsync,
    getProductsBycartAsync,
    getAllItemsAsync,
    getActiveCartDataAsync,
    getOldCartAsync,
    deleteItemAsync,
    deleteAllItemsAsync,
    updateCartAsync,
    insert,
    CheckCountDeliveryInDay,
    getActiveCartAsync
}