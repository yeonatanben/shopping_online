
const dal =require("../data-access-layer/dal");
const { builtinModules } = require("module");


function getAllProductsAsync(){
    const products= dal.executeQueryAsync("select * from products");
    return products;
}

function getCountProductAsync(){
    const count = dal.executeQueryAsync(`SELECT COUNT(productId) as sumproducts FROM products `);
    return count;
}

function getProductByIdAsync(id){ 
    const productById= dal.executeQueryAsync(`SELECT * FROM products WHERE productId=${id}`);
    return productById;

}
function getAllCategoriesAsync() {
    const categories = dal.executeQueryAsync("select * from category");
    return categories;
}





function getProductsByCategoryIdAsync(category){ 
    const productsByCategory= dal.executeQueryAsync(`SELECT * FROM products WHERE categoryId=${category}`);
    return productsByCategory;

}


function getProductByNameAsync(name){ 
    const productByName= dal.executeQueryAsync(`SELECT * FROM products WHERE productName like '%${name}%'`);
    return productByName;

}
 


module.exports={
    getAllProductsAsync,
    getCountProductAsync,
    getProductsByCategoryIdAsync,
    getProductByNameAsync,
    getProductByIdAsync,
    getAllCategoriesAsync
    
    
}