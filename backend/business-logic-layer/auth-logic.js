const jwt = require("jsonwebtoken");
const dal = require("../data-access-layer/dal");
const cryptoHelper = require("../helpers/crypto-helper");

async function loginAsync(credentials) {
    credentials.password = cryptoHelper.hash(credentials.password);
    const user = await dal.executeQueryAsync(
        `
        select * from users
        where mail=?
        and password=?
        `,
        [credentials.email, credentials.password]

    );
    if (!user || user.length < 1)
     return null;
    delete user[0].password;
    user[0].token = jwt.sign({ user: user[0] }, "yoni ben hur", { expiresIn: "55 minutes" });
    return user[0];
}

async function CheckUserId(id) {
    const userId = await dal.executeQueryAsync(
        `
        select * from users
        where userId=?
        `,
        [id]
    );

    if (!userId || userId.length < 1) return false;
    return true;
}

async function registerAsync(reg) {

    reg.password = cryptoHelper.hash(reg.password);
    const result = dal.executeQueryAsync(
        "insert into users values (?,?,?,?,?,?,?,?)", [reg.userId,reg.firstName,reg.lastName, reg.email,reg.password, reg.cityId,reg.street,""]
    );
    return result;

}








    module.exports = {
        loginAsync,
        registerAsync,
        CheckUserId,
    }