const express = require("express");
const expressFileUpload = require("express-fileupload");

const cors = require("cors");

const productsController = require("./controllers-layer/products-controller");
const adminController = require("./controllers-layer/admin-controller");
const authController = require("./controllers-layer/auth-controller");
const customersController=require("./controllers-layer/customers-controller");



const server = express();
server.use(cors());
server.use(expressFileUpload());
server.use(express.json());



const BASE_URL="/api/shopping"
server.use(BASE_URL+"/products", productsController);
server.use(BASE_URL+"/AdminArea", adminController);
server.use(BASE_URL+"/auth", authController);
server.use(BASE_URL+"/customers",customersController)

server.use(express.static("images"));


server.use("*", (req, res) => {
    res.status(404).send(`Route not found ${req.originalUrl}`);
});

server.listen(4000, () => {
    console.log("Listening on 4000");
}).on("error", (err) => {
    if (err.code === "EADDRINUSE")
        console.log("Error: Address in use");
    else
        console.log("Error: Unknown error");
});





