const { request, response } = require("express");
const Credentials = require("../model/Credentials");
const router = require("./products-controller");
const authLogic = require("../business-logic-layer/auth-logic");
const Registration = require("../model/Registration");

router.post("/login", async (request, response) => {
    try {
        const credentials = new Credentials(request.body);
        const errors = credentials.validate();
        if (errors)
         return response.status(400).send(errors);
        const loggedInUser = await authLogic.loginAsync(credentials);
        if (!loggedInUser)
         return response.status(201).json({message:"Incorect username or password"});
        response.send(loggedInUser);
    } catch (error) {
        response.status(500).send(error.message);
    }
});

router.post("/register", async (request, response) => {
    try {
        const reg = new Registration(request.body);
        const errors = reg.validate();
        if (errors) {
            console.log(errors);
            response.status(400).json(errors);
        }
        else {
            const result = await authLogic.registerAsync(reg);
            console.log(result);
            response.json(result);
        }
    } catch (error) {
        response.status(500).send(error.message);
    }
});

router.get("/check/:id", async (request, response) => {
    try {
        const id = request.params.id;
        const duplicateUser = await authLogic.CheckUserId(id);
        if (!duplicateUser)
            response.status(200).json('excellent');
        else
            response.status(200).json('id already exist');
    }
    catch (error) {
        response.status(500).send({ error: "server error" });
    }
});



module.exports = router;