const { request, response } = require("express");
const express = require("express");
const verifyLoggedIn = require("../middleware/verify-logged-in");
const productsLogic = require("../business-logic-layer/products-logic");
const VacationModel = require("../model/ProductModel");

const router = express.Router();
// מביא את כל המוצרים
router.get("/", verifyLoggedIn, async (request, response) => {
    try {
        const allProducts = await productsLogic.getAllProductsAsync();
        response.send(allProducts);
    }
    catch (error) {
        response.status(500).send(error);
    }
});
// מביא את כמות המוצרים באתר
router.get("/count", verifyLoggedIn, async (request, response) => {
    try {
        const count = await productsLogic.getCountProductAsync();
        response.send(count[0].sumproducts.toString());
    }
    catch (error) {
        response.status(500).send(error);
    }
});

//
router.get("/one/:id", verifyLoggedIn, async (request, response) => {
    try {

        const id = request.params.id;
        const product = await productsLogic.getProductByIdAsync(id);
        response.send(product[0]);
    } catch (error) {
        response.status(500).send(error);
    }
});
//מביא את כל הקטגוריות
router.get("/categories", verifyLoggedIn, async (request, response) => {
    try {
        const allCategories = await productsLogic.getAllCategoriesAsync();
        response.send(allCategories);
    }
    catch (error) {
        response.status(500).send(error);
    }
});



//מביא מוצרים לפי קטגוריה
router.get("/:id", verifyLoggedIn, async (request, response) => {
    try {

        const category = request.params.id;
        const products = await productsLogic.getProductsByCategoryIdAsync(category);
        response.send(products);
    } catch (error) {
        response.status(500).send(error);
    }
});
// מביא מוצר לפי שם
router.get("/name/:name", verifyLoggedIn, async (request, response) => {
    try {
        const name = request.params.name;
        const product = await productsLogic.getProductByNameAsync(name);
        response.send(product);
    } catch (error) {
        response.status(500).send(error);
    }
});




module.exports = router;