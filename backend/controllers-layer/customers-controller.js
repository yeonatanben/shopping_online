const express = require("express");
const customersLogic = require("../business-logic-layer/customers-logic");
const CartModel = require("../model/CartModel");
const OrderModel = require("../model/OrderModel");
const ItemInCartModel = require("../model/ItemInCartModel");
const verifyLoggedIn = require("../middleware/verify-logged-in");
const verifyAdmin = require("../middleware/verify-admin");




const router = express.Router();

router.get("/cities",
    // ,verifyLoggedIn
    async (request, response) => {
        try {
            const allCities = await customersLogic.getAllCitiesAsync();
            response.send(allCities);
        }
        catch (error) {
            response.status(500).send(error);
        }
    });
//להביא עגלה לפי יוזר
router.get("/cart/:id", verifyLoggedIn, async (request, response) => {
    const id = request.params.id;
    try {
        const cart = await customersLogic.getCartByIdAsync(id);
        response.send(cart);
    }
    catch (error) {
        response.status(500).send(error);
    }
});

//יצירת עגלה 
router.post("/cart", verifyLoggedIn, async (request, response) => {
    try {
        const cart = new CartModel(request.body);
        await customersLogic.insertCartAsync(cart);
        response.status(201).send({ ma: "cr" });
    } catch (error) {
        response.status(400).send(error);
    }
});

// לשנות סטטוס)עריכת  עגלה(
router.put("/cart", verifyLoggedIn, async (request, response) => {
    try {
        const id = request.body.cartId;
        await customersLogic.updateCartAsync(id);
        response.status(201).send({ Message: "update" });
    } catch (error) {
        response.status(400).send(error);
    }
});

//קבלת מספר ההזמנות באתר
router.get("/order", verifyLoggedIn, async (request, response) => {
    try {
        const countOrder = await customersLogic.getcountOrderAsync();
        response.send(countOrder[0].sumorder.toString());
    }
    catch (error) {
        response.status(500).send(error);
    }
});

//יצירת הזמנה 
router.post("/order", verifyLoggedIn, async (request, response) => {
    try {
        const order = new OrderModel(request.body);
        const date = request.body.dateDelivery.toString().slice(0, 10);
        const dayFull = await customersLogic.CheckCountDeliveryInDay(date);
        if (dayFull)
            response.status(201).send({ message: "The day is full" })
        else {
            await customersLogic.insertOrderAsync(order);
            response.status(201).json({ message: "created" });
        }
    } catch (error) {
        response.status(400).send({ error: "server error" });
    }
});
//להביא מוצרים לפי עגלה 
router.get("/products/:id", verifyLoggedIn, async (request, response) => {
    const cartId = request.params.id;
    try {
        const productsByCart = await customersLogic.getProductsBycartAsync(cartId);
        response.send(productsByCart);
    }
    catch (error) {
        response.status(500).send(error);
    }
});
//מביא מידע על עגלה פעילה לפי יוזר
router.get("/data/cartActive/:id", verifyLoggedIn, async (request, response) => {
    const id = request.params.id;
    try {
        const activeCartData = await customersLogic.getActiveCartDataAsync(id);
        response.send(activeCartData[0])
    }
    catch (error) {
        response.status(500).send(error);
    }
});

//מביא עגלה פעילה לפי יוזר
router.get("/cartActive/:id", verifyLoggedIn, async (request, response) => {
    const id = request.params.id;
    try {
        const activeCart = await customersLogic.getActiveCartAsync(id);
        response.send(activeCart[0])
    }
    catch (error) {
        response.status(500).send(error);
    }
});

//מביא עגלה ישנה לפי יוזר
router.get("/orderLast/:id", verifyLoggedIn, async (request, response) => {
    const id = request.params.id;
    try {
        const oldCart = await customersLogic.getOldCartAsync(id);
        response.send(oldCart[0]);
    }
    catch (error) {
        response.status(500).send(error);
    }
});
//מביא את כל המוצרים שבעגלה
router.get("/items/:id", verifyLoggedIn, async (request, response) => {
    const id = request.params.id;
    try {
        const allProducts = await customersLogic.getAllItemsAsync(id);
        response.send(allProducts);
    }
    catch (error) {
        response.status(500).send(error);
    }
});
// הוספת מוצר לעגלה 
router.post("/item", verifyLoggedIn, async (request, response) => {
    try {
        const item = new ItemInCartModel(request.body);
        const productId = await customersLogic.insert(item);
        if (!productId) {
            const addItem = await customersLogic.insertItemInCartAsync(item);
            response.status(201).send(addItem);
        }
        else {
            await customersLogic.updateItemInCartAsync(item);
            response.status(201).send();
        }
    } catch (error) {
        response.status(400).send(error);
    }

});
//למחוק מוצר מהעגלה
router.delete("/item/:id", verifyLoggedIn, async (request, response) => {
    const id = request.params.id;
    try {
        const deleteItem = await customersLogic.deleteItemAsync(id);
        response.send({ ma: "cr" });
    }
    catch (error) {
        response.status(500).send(error);
    }
});

router.delete("/cart/:id", verifyLoggedIn, async (request, response) => {
    const id = request.params.id;
    try {
        const deleteItem = await customersLogic.deleteAllItemsAsync(id);
        response.send({ ma: "cr" });
    }
    catch (error) {
        response.status(500).send(error);
    }
});





module.exports = router;