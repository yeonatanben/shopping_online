const express = require("express");
const verifyLoggedIn = require("../middleware/verify-logged-in");
const verifyAdmin = require("../middleware/verify-admin");
const adminLogic = require("../business-logic-layer/adminLogic");
const ProductModel = require("../model/ProductModel");



const fs = require("fs");
const path = require("path");




const router = express.Router();


//  הוספת מוצר
router.post("/",[verifyLoggedIn, verifyAdmin],async (request, response) => {
        try {
            const newProduct = new ProductModel(request.body);
            if (request.files?.imgPath) {
                newProduct.imgPath = await adminLogic.handleImage(request.files.imgPath);
            }
            await adminLogic.postProduct(newProduct);
            response.status(201).send({ messege: "created" });
        } catch (error) {
            console.log(error);
            response.status(400).send({ error: "server error" });
        }
    });

//  עריכת מוצר
router.put("/",[verifyLoggedIn, verifyAdmin], async (request, response) => {
    try {
        const editedProduct = new ProductModel(request.body);
        if (request.files?.imgPath) {
            editedProduct.imgPath = await adminLogic.handleImage(request.files.imgPath);
        }
        await adminLogic.editProduct(editedProduct);
        response.send({ messege: "edited" });
    } catch (error) {
        console.log(error);
        response.status(500).send({ error: "server error" });
    }
});



module.exports = router;
